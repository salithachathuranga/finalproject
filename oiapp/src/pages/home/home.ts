import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, LoadingController} from 'ionic-angular';
import {Chart} from 'chart.js';
import {DataServiceProvider} from "../../providers/data-service/data-service";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    @ViewChild("barChartCanvasArea") barChartCanvasArea: ElementRef;
    private bar_chart: Chart;
    private total_count: number = 0;
    private defect_count: number = 0;
    private defect_ratio: number = 0;
    private chart_labels: number[];
    private items: Object;
    private date: string;
    private nextTime: string;
    private prevTime: string;
    private config = {
        legend: {
            labels: {
                fontColor: "whitesmoke",
            }
        },
        scales: {
            yAxes: [
                {
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Count Per Minute',
                        fontColor: "whitesmoke"
                    },
                    ticks: {
                        fontColor: "whitesmoke",
                        beginAtZero: true,
                        min: 0,
                        max: 40,
                        stepSize: 5
                    }
                }
            ],
            xAxes: [
                {
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Minutes With Hour',
                        fontColor: "whitesmoke"
                    },
                    ticks: {
                        fontColor: "whitesmoke"
                    }
                }
            ]
        }
    }

    constructor(
        public loadingCtrl: LoadingController,
        public serviceProvider: DataServiceProvider,
        public alertCtrl: AlertController
    ) {
    }

    /* Function to draw bar chart */
    drawChart() {
        this.barChartCanvasArea.nativeElement.height = 450;
        this.bar_chart = new Chart(this.barChartCanvasArea.nativeElement, {
            type: "bar",
            data: {
                labels: this.chart_labels,
                datasets: [
                    {
                        label: "pass",
                        backgroundColor: "rgba(75, 192, 192, 0.2)",
                        borderWidth: 1,
                        data: this.items["pass"]
                    },
                    {
                        label: "fail",
                        backgroundColor: "rgba(255, 99, 132, 0.2)",
                        borderWidth: 1,
                        data: this.items["fail"]
                    }
                ]
            },
            options: this.config
        });
    }

    /* Create Loading Controller */
    loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });

    /* Load Data When Page Is Loaded */
    ionViewWillEnter() {
        this.getMessages();
        this.loading.dismiss();
    }

    ngOnInit() {
        this.loading.present();
    }

    /* Function to show alert */
    showError(error) {
        const alert = this.alertCtrl.create({
            title: error.statusText,
            subTitle: error.message,
            buttons: ['OK']
        });
        alert.present();
    }

    /* Function to get data from ES */
    getMessages() {
        this.serviceProvider.getDataFromEs()
            .subscribe(data => {
                if (data.hasOwnProperty('message')) {
                    this.showError(data);
                }
                else {
                    this.items = data;
                    this.date = data["date"];                    
                    this.defect_count = data["failCount"];                 
                    this.total_count = data["passCount"] + data["failCount"];
                    this.defect_ratio = Math.round((data["failCount"] / this.total_count) * 100);
                    this.prevTime = data["prevTime"];
                    this.chart_labels = this.items["minutes"];
                    this.nextTime = data["nextTime"];
                    this.drawChart();
                }
            }, error => {
                this.showError(error);
            });
    }

    /* Function to update bar chart */
    updateChart() {
        if (this.bar_chart) this.bar_chart.destroy();
        this.defect_count = 0;
	    this.defect_ratio = 0;
	    this.total_count = 0;
        this.barChartCanvasArea.nativeElement.height = 450;
        this.getMessages();
    }
}
