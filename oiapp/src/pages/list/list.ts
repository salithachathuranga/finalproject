import {Component} from '@angular/core';
import {AlertController, LoadingController} from "ionic-angular";
import {DataServiceProvider} from "../../providers/data-service/data-service";
import {Message} from "../../models/Message";

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {

    private items: Array<Message>;

    constructor(
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public serviceProvider: DataServiceProvider
    ) {
    }

    /* Create Loading Controller */
    loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });

    /* Load Data When Page Is Loaded */
    ionViewWillEnter() {
        this.getTopMessages();
        this.loading.dismiss();
    }

    ngOnInit() {
        this.loading.present();
    }

    /* Function to show alert */
    showError(error) {
        const alert = this.alertCtrl.create({
            title: error.statusText ? error.statusText: null,
            subTitle: error.message,
            buttons: ['OK']
        });
        alert.present();
    }

    /* Function to get top 100 data from ES */
    getTopMessages() {
        this.serviceProvider.getAllDataFromEs()
            .subscribe(data => {
                data.hasOwnProperty('message') ? this.showError(data) : this.items = data['response'];
            }, error => {
                this.showError(error);
            });
    }
}
