export interface Message {
  id: number,
  status: string;
  date: string,
  hour: number,
  min: number
}
