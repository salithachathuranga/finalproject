import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class DataServiceProvider {

    constructor(public http: HttpClient) {
    }

    /* Function to call API to get events per minute */
    getDataFromEs() {
        return this.http.get("http://localhost:9000/emit_hour_msgs");
    }

    /* Function to call API to get top 100 events from ES */
    getAllDataFromEs() {
        return this.http.get("http://localhost:9000/emit_top_msgs");
    }

}
