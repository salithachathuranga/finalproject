
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController } from 'ionic-angular';
import { ConnectionStatus } from '../../Models/ConnectionStatus';


@Injectable()
export class NetworkProvider {

  /** network status-
   * from Models/ConnectionStatus-enum 
   * */
  public status: ConnectionStatus;

  constructor(private network: Network,private alertCtrl: AlertController) {

    this.status = ConnectionStatus.Online;
  }

  /**---- initialize network status ------ */
  public initializeNetwork(): void {

    /** OFFLINE */
    this.network.onDisconnect().subscribe(() => {
        if (this.status === ConnectionStatus.Online) {
            this.setStatus(ConnectionStatus.Offline);
        }
        this.presentAlert("Network Connection","You Are Offline");
    });

    /** ONLINE */
    this.network.onConnect().subscribe(() => {
        if (this.status === ConnectionStatus.Offline) {
            this.setStatus(ConnectionStatus.Online);
        }
        this.presentAlert("Network Connection","You Are Online");
    });

  }

  /**---- set network status function ------ */
  private setStatus(status: ConnectionStatus) {
    this.status = status;
  }

  /**---- get network status function ------ */
  public getNetworkStatus(): ConnectionStatus {
    return this.status;
  }

   /**---- Alert function ------ */
   presentAlert(alertTitle,alertMessage) {
    let alert = this.alertCtrl.create({
      title: alertTitle,
      message: alertMessage,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
