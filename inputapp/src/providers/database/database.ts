import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, AlertController} from 'ionic-angular';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  private databaseObj: SQLiteObject;

  /** Database name */
  readonly database_name: string = "factory_count.db";

  /** db table names */
  readonly pass_table: string = "pass_count";
  readonly fail_table: string = "fail_count";
  readonly offline_pass_table: string = "offline_pass_message";
  readonly offline_fail_table: string = "offline_fail_message";
 
  constructor(
    public http: HttpClient,
    private platform: Platform,
    private sqlite: SQLite,
    private alertCtrl: AlertController,) {
 
    this.platform.ready().then(() => {
      this.createDB();  
    }).catch(error => {
      this.presentAlert("Error ","Database creating Error!");
    });

  }

    /**  Db creating */
  createDB() {
    this.sqlite.create({
      name: this.database_name,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db;
      }).then(() => {
        this.createCountTable(this.pass_table); // create pass Count Table
        this.createCountTable(this.fail_table); // create fail Count Table
        this.createOffLineTable(this.offline_pass_table); // create ooffline pass message Table
        this.createOffLineTable(this.offline_fail_table); // create ooffline fail message Table
      })
      .catch(e => {
        this.presentAlert("Table Creating! ","Table Creating Error! ");
      })
  }

  /**  Table droping function (if you need) */
  dropTable(table:string) {
    this.databaseObj.executeSql(`
    DROP TABLE ${table}
    `, [])
      .then(() => {
        this.presentAlert("Table Dropped! ","Table Droped");
      })
      .catch(e => {
        this.presentAlert("Table Dropped! ","Error");
      });
  }

  /**  Create count table function */
  createCountTable(table:string) {
    this.databaseObj.executeSql(`
    CREATE TABLE IF NOT EXISTS ${table} (id INTEGER PRIMARY KEY AUTOINCREMENT,count INT)
    `, [])
      .catch(e => {
        this.presentAlert("Table Creating! ","Table Creating Error");
      });
  }

  /**  Create offline table function */
  createOffLineTable(table:string) {
    this.databaseObj.executeSql(`
    CREATE TABLE IF NOT EXISTS ${table} (id INTEGER PRIMARY KEY,status TEXT,date TEXT,
      hour int,min INT,sent INT DEFAULT 0)
    `, [])
      .catch(e => {
        this.presentAlert("Table Creating! ","Table Creating Error");
      });
  }

  /**  Insert count for given table */
  insertCount(table:string,count:number) {
    
    this.databaseObj.executeSql(`
      INSERT INTO ${table} (count) VALUES ('${count}')
    `, [])
      .catch(e => {
        this.presentAlert("Insert Error! ","Count inserting error");
      });
  }

  /**  Insert offline message */
  insertOffLineMessage(table:string,id:number,status:string,date:string,hour:number,min:number) {
    
    this.databaseObj.executeSql(`
      INSERT INTO ${table} (id,status,date,hour,min) VALUES ('${id}','${status}','${date}','${hour}','${min}')
    `, [])
      .catch(e => {
        this.presentAlert("Insert Error! ","OffLine message Insert Error"+JSON.stringify(e));
      });
  }

  /**  Get count for given table */
  getCount(table:string) {

    return this.databaseObj.executeSql(`
      SELECT count FROM ${table}
      `, [])
        .then((res) => {
          if(res.rows.length == 0){
            return 0;
          }
          else{
            return res.rows.item(0).count;
          }
        })
        .catch(error => {
          this.presentAlert("Count Getting Error! ","Count Checking Error!");
        });
  }

  /**  Update count for given table */
  updateCount(table:string,count:number) {
    this.databaseObj.executeSql(`
      UPDATE ${table}
      SET count = '${count}' 
    `, [])
      .catch(e => {
        this.presentAlert("Updating Error! ","Count updating error");
      });
  }

  /**  Get count for given table */
  getOfflineMessage(table:string) {

    return this.databaseObj.executeSql(`
      SELECT * FROM ${table} WHERE sent=0
      `, [])
        .then((data) => {
          let OfflineMessage= [];
          if(data.rows.length > 0){
            for(let i=0; i <data.rows.length; i++) {
              OfflineMessage.push({
                id:data.rows.item(i).id,
                status:data.rows.item(i).status,
                date:data.rows.item(i).date,
                hour:data.rows.item(i).hour,
                min:data.rows.item(i).min
              });
            }
            return OfflineMessage;
          }
          else{
            return 0;
          }
        })
        .catch(error => {
          this.presentAlert("Offline Messages Getting Error! ","get error"+JSON.stringify(error));
        });
  }

  /**  Update count for given table */
  updateOfflineMessageSending(table:string, id:number) {
    this.databaseObj.executeSql(`
      UPDATE ${table}
      SET sent = 1 
      WHERE id = ${id}
    `, [])
      .catch(error => {
        this.presentAlert("Offline mMessages Updating Error! ",JSON.stringify(error));
      });
  }


  /**---- Alert function ------ */
  presentAlert(alertTitle:string, alertMessage:string) {
    let alert = this.alertCtrl.create({
      title: alertTitle,
      message: alertMessage,
      buttons: ['Dismiss']
    });
    alert.present();
  }


}
