import { Component } from '@angular/core';
import { NavController,AlertController,Platform } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { DatePipe } from '@angular/common'; 
import { DatabaseProvider } from '../../providers/database/database';
import { NetworkProvider } from '../../providers/network/network';
import { Message } from '../../Models/Message';
import { ConnectionStatus } from '../../Models/ConnectionStatus';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  private headers: HttpHeaders;

  /** Quality status */
  private pass:string = "PASS";
  private fail:string = "FAIL";

  /** pass/fail Count variable */
  public pass_count;
  public fail_count;

  /** db table names */
  private pass_table: string = "pass_count";
  private fail_table: string = "fail_count";
  private offline_pass_table: string = "offline_pass_message";
  private offline_fail_table: string = "offline_fail_message";

  /** Message sending url */
  private send_url = "http://e17c549b.ngrok.io/input";

  constructor(
    private http: HttpClient,
    private database: DatabaseProvider,
    private platform: Platform,
    private alertCtrl: AlertController,
    private datePipe: DatePipe,
    private network:NetworkProvider,
    public navCtrl: NavController) {

    this.headers = new HttpHeaders();
    this.headers.append("Accept", 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('responseType', 'text');
    this.headers.append('Access-Control-Allow-Origin', 'HTTP://localhost:9000'); 

    this.network.initializeNetwork();

  }

  /**-----getting and initilaizing last pass/fail count after page is loaded-------*/
  ionViewDidLoad() {
    this.platform
      .ready()
      .then(() =>
      {
        setTimeout(() =>
        {
          this.countInit(); 
        }, 1000);
      });
  }

  /** --- initilize function for last pass/fail count ---*/
  countInit(){
    this.database.getCount(this.pass_table).then((result) => { 
      this.pass_count =  result;  
    });

    this.database.getCount(this.fail_table).then((result) => {  
      this.fail_count =  result;
    });
  }

    /**-- Pass Button-- set/send message */
  passButton(){ 

    let CURRENT_DATE = new Date();
    let minutes = CURRENT_DATE.getMinutes();
    let hour = CURRENT_DATE.getHours();
    let today = this.transformDate(CURRENT_DATE);

    let message = new Message(this.pass_count+1, this.pass, today, hour ,minutes);

    this.sendAndChecking(message,this.pass_table, this.pass_count, this.pass_count+1); 

  }

    /**-- Fail button-- set/send message ----*/
  failButton(){

    let CURRENT_DATE = new Date();
    let minutes = CURRENT_DATE.getMinutes();
    let hour = CURRENT_DATE.getHours();
    let today = this.transformDate(CURRENT_DATE);

    let message = new Message(this.fail_count+1, this.fail, today, hour ,minutes);

    this.sendAndChecking(message,this.fail_table, this.fail_count, this.fail_count+1);   
  }


    /**----- checking network connection and updating ----*/
  sendAndChecking(message:Message,table:string, lastCount:number, newCount:number){

    let status = message.getStatus();

    if(this.network.getNetworkStatus() === ConnectionStatus.Online){

      this.sendMessage(message);

      if(status == this.pass){
        this.sendOffLineMessages(this.offline_pass_table);
      }
      else{
        this.sendOffLineMessages(this.offline_fail_table);
      }

    }
    else if(this.network.getNetworkStatus() === ConnectionStatus.Offline){

      this.presentAlert("Network Offline","You Are Offline");  
      this.insertIntoOffline(message); 
    }

    if(status == this.pass){
      this.pass_count++;
    }
    else{
      this.fail_count++;
    }

    this.countForDb(table, lastCount, newCount)

  }

  /**----- message sending ----*/  
  sendMessage(message:Message){

    this.http.post(this.send_url, message,{headers: this.headers})
      .subscribe(response => {

      },error=>{
        this.insertIntoOffline(message);
      });
  }

    /**----- insert offline messages to offline tables ----*/  
  insertIntoOffline(message:Message){

    let id = message.getId();
    let status = message.getStatus();
    let date = message.getDate();
    let hour = message.getHour();
    let min = message.getMin();

    if(status == this.pass){
      this.database.insertOffLineMessage(this.offline_pass_table, id, status, date, hour, min);
    }
    else{
      this.database.insertOffLineMessage(this.offline_fail_table, id, status, date, hour, min);
    }
  }

  /**----- send offline messages ----*/
  sendOffLineMessages(table:string){
    
    this.database.getOfflineMessage(table).then((result) => { 

      let messages;
      if(result != 0){

        messages =  result;  

        for(let i=0; i <messages.length; i++) {

          let message = new Message(messages[i].id,
            messages[i].status, messages[i].date,
            messages[i].hour,messages[i].min);

          this.http.post(this.send_url, message,{headers: this.headers})
            .subscribe(response => {
              this.database.updateOfflineMessageSending(table, message.getId());
            },error=>{
            });
     
        }
      }   
    });

  }

   /**----- Insert/Update count increment for db---- */
   countForDb(table:string, lastCount:number, newCount:number){
    
    if(lastCount == 0){
      this.database.insertCount(table, newCount);    
    }
    else{
      this.database.updateCount(table, newCount);
    }
  }

  /**---- Date formating function ------ */
  transformDate(date):string {
    return this.datePipe.transform(date, 'yyyy-MM-dd'); 
  }

  /**---- Alert function ------ */
  presentAlert(alertTitle:string,alertMessage:string) {
    let alert = this.alertCtrl.create({
      title: alertTitle,
      message: alertMessage,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
