export class Message {

    public id: number;
    private status: string;
    private date: string;
    private hour:number;
    private min:number;

    constructor(
        id: number,
        status: string,
        date: string,
        hour:number,
        min:number,
    ){
        this.setId(id);
        this.setStatus(status);
        this.setDate(date);
        this.setHour(hour);
        this.setMin(min);
    }

    setId(id){
        this.id = id;
    }

    public getId(){
        return this.id;
    }

    setStatus(status){
        this.status = status;
    }

    public getStatus(){
        return this.status;
    }
    setDate(date){
        this.date = date;
    }

    public getDate(){
        return this.date;
    }

    setHour(hour){
        this.hour = hour;
    }

    public getHour(){
        return this.hour;
    }

    setMin(min){
        this.min = min;
    }

    public getMin(){
        return this.min;
    }


}