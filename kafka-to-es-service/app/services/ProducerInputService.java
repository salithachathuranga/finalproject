package services;

import bo.QualityCheckedResultModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Singleton;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;
import play.libs.Json;

import java.util.Properties;

@Singleton
public class ProducerInputService {

    public static String TOPIC = "quality-check-topic";

    private static Logger logger;

    public ProducerInputService(){
        logger = Logger.getLogger(this.getClass());
    }


    /**
     * Method to send Json object to kafka topic by using kafka producer
     */
    public JsonNode sendMessage(JsonNode body) {

        logger.info("Input Json object : "+body);

        ObjectNode resultsJson = Json.newObject();
        if(!isValidInput(body)){
            resultsJson.put("id","ERROR");
            return resultsJson;
        }
        else{

                int id = body.get("id").asInt();
                String status = body.get("status").asText();
                String date= body.get("date").asText();
                int hour= body.get("hour").asInt();
                int min= body.get("min").asInt();

                QualityCheckedResultModel qualityCheckedResultModel = new QualityCheckedResultModel(id,status,date,hour,min);

                Properties properties = new Properties();
                properties.put("bootstrap.servers", "localhost:9092");

            /**
             * Create kafka producer
             */

                KafkaProducer<String,QualityCheckedResultModel> producer =
                        new KafkaProducer<>(
                                properties,
                                new StringSerializer(),
                                new KafkaJsonSerializer()
                        );

                ProducerRecord<String, QualityCheckedResultModel> record = new ProducerRecord<>(TOPIC, qualityCheckedResultModel);
            try {
                producer.send(record);
                return Json.toJson(qualityCheckedResultModel);

            } catch (Exception e) {

                resultsJson.put("error",e.getMessage());
                return resultsJson;
            }finally{
                producer.flush();
                producer.close();
            }
        }



    }


    /**
     * Method to validate the Json object
     */
    private boolean isValidInput(JsonNode body){

        if(body.isEmpty())
            return false;
        else if(!body.has("id") || !body.has("status") ||
                !body.has("date")|| !body.has("hour") || !body.has("min"))
            return false;
        else if(body.get("id").isNull() || body.get("status").isNull() ||
                body.get("date").isNull() || body.get("hour").isNull() || body.get("min").isNull())
            return false;
        else
            return true;

    }



}
