package services;


import bo.QualityCheckedResultModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import play.libs.Json;


import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;


public class ConsumerService {


    public static String TOPIC = "quality-check-topic";
    private final String INDEX_NAME = "es-quality-checking";
    private final String DOC_TYPE = "qualitycheck";
    private final int PORT = 9200;
    private final String HOST = "localhost";

    /**
     * Set the http client configuration options
     */

    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost(HOST, PORT));
    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    private static Logger logger;

    public ConsumerService(){
        logger = Logger.getLogger(this.getClass());
    }


    public JsonNode subscribeMessage(JsonNode body) {

        logger.info(" Subscribed Json object kafka topic : "+body);

        /**
         * Set properties for kafka consumer
         */
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);


        ObjectNode result = Json.newObject();


        /**
         * Create kafka consumer
         */
        KafkaConsumer<String, QualityCheckedResultModel> consumer =
                new KafkaConsumer<>(
                        properties,
                        new StringDeserializer(),
                        new KafkaJsonDeserializer<QualityCheckedResultModel>(QualityCheckedResultModel.class));


        /**
         * Subscribe to the kafka topic
         */
        consumer.subscribe(Collections.singletonList(TOPIC));

        try {

            if (body.get("id").asText()=="ERROR") {
                result.put("error", "invalid inputs");
            } else {
                ConsumerRecords<String, QualityCheckedResultModel> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                consumerRecords.forEach(record -> {
                    int id = body.get("id").asInt();
                    String status = body.get("status").asText();
                    String date = body.get("date").asText();
                    int hour = body.get("hour").asInt();
                    int min = body.get("min").asInt();
                    QualityCheckedResultModel message = new QualityCheckedResultModel(id, status, date, hour, min);

                    /**
                     * Connect the kafka consumer to send the object to ES
                     * */
                    IndexRequest request = new IndexRequest(INDEX_NAME).type(DOC_TYPE)
                            .source(Json.stringify(Json.toJson(message)), XContentType.JSON);
                    IndexResponse response = null;

                    try {
                        response = highLevelClient.index(request, RequestOptions.DEFAULT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (response.getIndex() != null && response.status() == RestStatus.CREATED) {
                        result.put("message", "Message Created ID: " + message.getId());
                    } else {
                        result.put("message", "Message Not Created");
                    }


                });
                consumer.commitSync();
            }
            }catch(Exception e){
                result.put("error", e.getMessage());
            }
            finally{
                consumer.close();
                return result;
            }


    }

}
