package services.dto;

public class StatusAggregator {
    public long count;
    public String key;

    public StatusAggregator(long count, String key) {
        this.count = count;
        this.key = key;
    }
    public StatusAggregator(){}

    @Override
    public String toString() {
        return "DateAggregator{" +
                "count=" + count +
                ", key='" + key + '\'' +
                '}';
    }
}
