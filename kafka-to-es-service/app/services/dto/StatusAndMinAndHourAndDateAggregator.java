package services.dto;

import java.util.List;

public class StatusAndMinAndHourAndDateAggregator {
    public long count;
    public String key;
    public List<StatusAndMinAndHourAggregator> values;

    public StatusAndMinAndHourAndDateAggregator(long count, String key, List<StatusAndMinAndHourAggregator> values) {
        this.count = count;
        this.key = key;
        this.values = values;
    }

    @Override
    public String toString() {
        return "DateAndStatusAndHourAndMinAggregator{" +
                "count=" + count +
                ", key='" + key + '\'' +
                ", values=" + values +
                '}';
    }
}
