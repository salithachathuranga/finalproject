package services.dto;

import java.util.List;

public class StatusAndMinAndHourAggregator {
    public long count;
    public String key;
    public List<StatusAndMinAggregator> values;

    public StatusAndMinAndHourAggregator(long count, String key, List<StatusAndMinAggregator> values) {
        this.count = count;
        this.key = key;
        this.values = values;
    }

    @Override
    public String toString() {
        return "DateAndStatusAndHourAggregator{" +
                "count=" + count +
                ", key='" + key + '\'' +
                ", values=" + values +
                '}';
    }
}
