package services;

import bo.QualityCheckedResultModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import play.libs.Json;
import services.dto.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class MessageService {


    private final String INDEX_NAME = "es-quality-checking";
    private final String DOC_TYPE = "qualitycheck";
    private final int PORT = 9200;
    private final String HOST = "localhost";
    private static Logger logger;

    public MessageService(){
        logger = Logger.getLogger(this.getClass());
    }

    /**
     *  Set the http client configuration options
     */
    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost(HOST, PORT));
    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );


    /**
     * Method to get top records from ES
     */
    public JsonNode getTopRecords() {
        ObjectNode resultJson = Json.newObject();
        List<QualityCheckedResultModel> results = new ArrayList<>();
        try {
            SearchSourceBuilder query = new SearchSourceBuilder().query(
                    QueryBuilders.termQuery("date", String.valueOf(LocalDate.now())
                    )).sort("hour", SortOrder.DESC).sort("min", SortOrder.DESC);
            SearchRequest request = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(query.from(0).size(100));
            SearchResponse response = highLevelClient.search(request, RequestOptions.DEFAULT);
            if (response == null) {
                resultJson.put("message", "Result is NULL");
            }
            else if (!(response.status() == RestStatus.OK)) {
                resultJson.put("message", "Request is not proceeded!");
            }
            else {
                for (SearchHit documentFields : response.getHits().getHits()) {
                    String sourceAsString = documentFields.getSourceAsString();
                    QualityCheckedResultModel document = Json.fromJson(Json.parse(sourceAsString), QualityCheckedResultModel.class);
                    results.add(document);
                }
            }
        } catch (ElasticsearchStatusException | IOException e) {
            resultJson.put("message", e.getMessage());
        }
        resultJson.putPOJO("response", results);
        return resultJson;
    }

    /**
     * Method to return aggregated result of pass and fail counts.
     */
    public JsonNode aggregate() throws Exception {
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        int hour = localTime.getHour();
        int minute = localTime.getMinute();
        int gteHour, ltHour, gteMinute;
        if (minute == 0) {
            gteMinute = 59;
            gteHour = hour - 2;
            ltHour = hour - 1;
        } else {
            gteMinute = minute - 1;
            gteHour = hour - 1;
            ltHour = hour;
        }
        ObjectNode finalResult = Json.newObject();
        try {
            SearchSourceBuilder query = new SearchSourceBuilder()
                    .query(QueryBuilders.boolQuery().filter(
                            QueryBuilders.termQuery("date", String.valueOf(localDate))
                    ).must(QueryBuilders.boolQuery().should(QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("hour").gte(gteHour).lt(ltHour)
                    ).must(QueryBuilders.rangeQuery("min").gt(gteMinute)))
                            .should(QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("hour").gt(gteHour).lte(ltHour)
                            ).must(QueryBuilders.rangeQuery("min").lte(gteMinute)))))
                    .aggregation(
                            AggregationBuilders.terms("DATE_AGGREGATION").field("date")
                                    .subAggregation(AggregationBuilders.terms("HOUR_AGGREGATION").field("hour")
                                            .subAggregation(AggregationBuilders.terms("MIN_AGGREGATION").field("min")
                                                    .subAggregation(AggregationBuilders.terms("STATUS_AGGREGATION").field("status"))))
                    );

            SearchRequest request = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(query.from(0).size(0));
            SearchResponse response = highLevelClient.search(request, RequestOptions.DEFAULT);
            if (response == null) {
                finalResult.put("message", "Result is NULL!");
            }
            else if (!(response.status() == RestStatus.OK)) {
                finalResult.put("message", "Request is not proceeded!");
            }
            else {
                List<StatusAndMinAndHourAndDateAggregator> resultList = aggregationParser(response);
                if (resultList.size() == 0) {
                    finalResult.put("message", "No Results For This Period!");
                    return finalResult;
                }
                else {
                    List<JsonNode> messageObjectList = processEsDataObject(resultList, localDate.toString());
                    List<String> labelsList = new ArrayList<>();
                    List<Integer> failCountList = new ArrayList<>();
                    List<Integer> passCountList = new ArrayList<>();
                    for (JsonNode ob : messageObjectList) {
                        labelsList.add(ob.get("hour").asText()+":"+ob.get("minute").asText());
                        passCountList.add(ob.get("passCount").asInt());
                        failCountList.add(ob.get("failCount").asInt());
                    }
                    int passCount = passCountList.stream().reduce(0, Integer::sum);
                    int failCount = failCountList.stream().reduce(0, Integer::sum);

                    finalResult.putPOJO("minutes", labelsList);
                    finalResult.putPOJO("pass", passCountList);
                    finalResult.put("passCount", passCount);
                    finalResult.putPOJO("fail", failCountList);
                    finalResult.put("failCount", failCount);
                    finalResult.put("date", localDate.toString());

                    String timeDuration1 = gteHour >= 12 ? "PM" : "AM";
                    String timeDuration2 = ltHour >= 12 ? "PM" : "AM";
                    finalResult.put("prevTime", (gteHour) + ":" + (gteMinute == 59 ? "00" : ((gteMinute+1) >= 10 ? (gteMinute+1) : ("0"+(gteMinute+1)))) + timeDuration1);
                    finalResult.put("nextTime", (gteMinute == 59 ? (ltHour-1) : ltHour) + ":" + (gteMinute == 59 ? (gteMinute) : ((gteMinute) >= 10 ? (gteMinute) : ("0"+(gteMinute)))) + timeDuration2);
                }
            }
        } catch (ElasticsearchStatusException | IOException e) {
            finalResult.put("message", e.getMessage());
        }
        logger.info(" Pass and fail count for particular minute : "+finalResult);
        return finalResult;
    }

    /**
     * Method to process Es data objects to get pass or fail count for particular minute
     */
    private List<JsonNode> processEsDataObject(List<StatusAndMinAndHourAndDateAggregator> resultList, String date){
        List<JsonNode> messageObjectList = new ArrayList<>();

        for (StatusAndMinAndHourAndDateAggregator StatusAndMinAndHourAndDateAggregatorObject: resultList) {

            if(StatusAndMinAndHourAndDateAggregatorObject.key.equals(date)){
                List<StatusAndMinAndHourAggregator> StatusAndMinAndHourAggregatorList = StatusAndMinAndHourAndDateAggregatorObject.values;
                for (StatusAndMinAndHourAggregator StatusAndMinAndHourAggregatorObject: StatusAndMinAndHourAggregatorList) {
                    List<StatusAndMinAggregator> StatusAndMinAggregatorList = StatusAndMinAndHourAggregatorObject.values;

                    for (StatusAndMinAggregator StatusAndMinAggregatorObject: StatusAndMinAggregatorList) {
                        List<StatusAggregator> StatusAggregatorList = StatusAndMinAggregatorObject.values;

                        ObjectNode messageObject = Json.newObject();
                        int passCount = 0;
                        int failCount = 0;
						messageObject.put("hour", StatusAndMinAndHourAggregatorObject.key);
                        messageObject.put("minute", StatusAndMinAggregatorObject.key);

                        for (StatusAggregator StatusAggregatorObject: StatusAggregatorList) {
                            if (StatusAggregatorObject.key.equals("pass")){
                                passCount = (int) StatusAggregatorObject.count;
                            }
                            else {
                                failCount = (int) StatusAggregatorObject.count;
                            }
                            messageObject.put("passCount", passCount);
                            messageObject.put("failCount", failCount);
                        }
                        messageObjectList.add(messageObject);
                        logger.info(" Pass and fail count : "+messageObject);
                    }
                }
            }
        }
        return messageObjectList;
    }

    /**
     * Method to parser SearchResponse aggregations into JAVA DTO which can be directly serialize to JSON data in API response
     */
    private List<StatusAndMinAndHourAndDateAggregator> aggregationParser(SearchResponse searchResponse) {

        List<StatusAndMinAndHourAndDateAggregator> valuesStatusAndMinAndHourAndDateAggregator = new ArrayList<>();
        MultiBucketsAggregation aggregations = searchResponse.getAggregations().get("DATE_AGGREGATION");

        for (MultiBucketsAggregation.Bucket bucket : aggregations.getBuckets()) {
            String dateKey = bucket.getKey().toString();
            long dateCount = bucket.getDocCount();
            MultiBucketsAggregation internalHourBuckets = bucket.getAggregations().get("HOUR_AGGREGATION");
            List<StatusAndMinAndHourAggregator> valuesStatusAndMinAndHourAggregator = new ArrayList<>();

            for (MultiBucketsAggregation.Bucket internalStatusBucket : internalHourBuckets.getBuckets()) {
                String hourKey = internalStatusBucket.getKey().toString();
                long hourCount = internalStatusBucket.getDocCount();
                MultiBucketsAggregation internalMinBuckets = internalStatusBucket.getAggregations().get("MIN_AGGREGATION");
                List<StatusAndMinAggregator> valuesStatusAndMinAggregator = new ArrayList<>();

                for (MultiBucketsAggregation.Bucket internalHourBucket : internalMinBuckets.getBuckets()) {
                    String minKey = internalHourBucket.getKey().toString();
                    long minCount = internalHourBucket.getDocCount();
                    MultiBucketsAggregation internalStatusBuckets = internalHourBucket.getAggregations().get("STATUS_AGGREGATION");
                    List<StatusAggregator> valuesStatusAggregator = new ArrayList<>();

                    for (MultiBucketsAggregation.Bucket internalMinBucket : internalStatusBuckets.getBuckets()) {
                        String statusKey = internalMinBucket.getKey().toString();
                        long statusCount = internalMinBucket.getDocCount();
                        valuesStatusAggregator.add(new StatusAggregator(statusCount, statusKey));
                    }
                    valuesStatusAndMinAggregator.add(new StatusAndMinAggregator(minCount, minKey, valuesStatusAggregator));
                }
                valuesStatusAndMinAndHourAggregator.add(new StatusAndMinAndHourAggregator(hourCount, hourKey, valuesStatusAndMinAggregator));
            }
            valuesStatusAndMinAndHourAndDateAggregator.add(new StatusAndMinAndHourAndDateAggregator(dateCount, dateKey, valuesStatusAndMinAndHourAggregator));
        }
        return valuesStatusAndMinAndHourAndDateAggregator;
    }
}
