package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.libs.Json;
import play.mvc.*;
import services.ConsumerService;
import services.MessageService;
import services.ProducerInputService;



public class KafkaInputController extends Controller {

    @Inject
    public ProducerInputService producerInputService;

    @Inject
    public ConsumerService consumerService;

    @Inject
    public MessageService messageService;

    public Result postMessage(Http.Request request)  {

        JsonNode result = consumerService.subscribeMessage(producerInputService.sendMessage(request.body().asJson()));

        return ok(result);
    }
    public Result aggregate() throws Exception {
        JsonNode results = messageService.aggregate();
        return Results.ok(Json.toJson(results));
    }

    public Result getTopRecords() throws Exception {
        JsonNode results = messageService.getTopRecords();
        return Results.ok(Json.toJson(results));
    }


}
