package bo;

public class QualityCheckedResultModel {

    private int id;
    private String status;
    private String date;
    private int hour;
    private int min;

    public QualityCheckedResultModel(){}

    public QualityCheckedResultModel(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public QualityCheckedResultModel(int id, String status, String date, int hour, int min) {
        this.id = id;
        this.status = status;
        this.date = date;
        this.hour = hour;
        this.min = min;
    }

    public int getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
